/***********************************************************************

Nom ......... : user_mainfile.c
Role ........ : Example of user function definition
Auteur ...... : Julian Mendez <julian.mendez@cern.ch>
Version ..... : V1.0 - 25/10/2017

Compilation :
	- Add the file into the FILES list located in ipmc-user/nr-mkfrag

***********************************************************************/
#error "ipmc-user/user_mainfile.c: This file is an example. Either delete the file or remove this line if you want to use the file"

#include <defs.h>
#include <cfgint.h>
#include <app.h>

/* Required to control I/Os */
#include <app/signal.h>

/* INIT_CALLBACK(fctname) is called during the IPMC initialisation */
INIT_CALLBACK(usermain_init)
{
    /* Init readback var. */
    char rd;

    /* Set signal information in a variable */
    /** User I/O defines:

    The user I/O can be:
        USER_IO_0_ACTL to USER_IO_34_ACTL  : User I/O pin in inverted mode (active: GND / inactive: VCC)
        IPM_IO_0_ACTL to IPM_IO_15_ACTL    : IPM I/O pin in inverted mode (active: GND / inactive: VCC)
        USER_IO_0_ACTH to USER_IO_34_ACTH  : User I/O pin in direct mode (active: VCC / inactive: GND)
        IPM_IO_0_ACTH to IPM_IO_15_ACTH    : IPM I/O pin in direct mode (active: VCC / inactive: GND)

    By default, the value of
        USER_IO_0 to USER_IO_34  : is connected to _ACTL signals, meaning that these defines are active low
        IPM_IO_0 to IPM_IO_15    : is connected to _ACTL signals, meaning that these defines are active low
    */

    signal_t userio_sig = USER_IO_0;
    signal_t userio_sig_actl = USER_IO_1_ACTL;
    signal_t userio_sig_acth = USER_IO_2_ACTH;

    /** The macro definition below show an example of the I/O
     * definition. It links the macro to the firmware, which
     * finally link it to an FPGA pin routed to the dedicated
     * I/O on the edge connector.
     *
     *	#define USER_IO_0					\
     *   {									\
     *		bufferPadAddr: USRIO_BUFFPAD,   \
     *		bitPosInBufferPad: 0, 			\
     *		pin: P(3, 16),					\
     *		active: SIGNAL_LOW,				\
     *		inactive: SIGNAL_HIGH  			\
     *   }
     *
     * The active (1) and inactive (0) states are inverted.
     * Therefore, activating a signal set it to GND and
     * de-activating it set it to VCC. This inversion remains
     * from an old version to avoid compatibility issues with
     * new binaries.
     **/

    /* Set signal to output and high level */
    signal_activate(&userio_sig);      // User I/O_0 is set to GND
    signal_activate(&userio_sig_actl); // User I/O_1 is set to GND
    signal_activate(&userio_sig_acth); // User I/O_2 is set to VCC

    /* Set signal to output and high level */
    signal_deactivate(&userio_sig);

    /* Set signal in input mode (default mode) */
    signal_set_pin(&userio_sig, SIGNAL_HIGHZ);

    /* Read the signal value (returned value)*/
    rd = signal_read(&userio_sig);
    if(rd == 1){
        printf("Pin is activated (GND)");
    }else{
        printf("Pin is de-activated (VCC)");
    }
}

unsigned char io_sate_mainloop = 0;

/* MAIN_LOOP_CALLBACK(fctname) is called at every execution of the main loop */
MAIN_LOOP_CALLBACK(usermain_mainloop)
{
      /* 
      This block of code illustrates how the FRU LEDs can be used
      Note: This use of the LEDs may interfere with the “override” and “lamp test” modes of the LEDs defined by PICMG
      signal_t signal_fru1 = CFG_FRU_LED_1_SIGNAL;   
      signal_t signal_fru2 = CFG_FRU_LED_2_SIGNAL;      
      signal_t signal_fru3 = CFG_FRU_LED_3_SIGNAL;    
      static int waitabit = 0;
      static char led_toggle = 0;
      char sig_fru1, sig_fru2, sig_fru3;

      waitabit++;               //Just to reduce the blinking frequency
      if (waitabit == 1000)
      {
	sig_fru1 = signal_read(&signal_fru1);
	sig_fru2 = signal_read(&signal_fru2);
	sig_fru3 = signal_read(&signal_fru3);
	debug_printf("MAIN_LOOP_CALLBACK: fru1 = %d, fru2 = %d, fru3 = %d\n\r", sig_fru1, sig_fru2, sig_fru3);

	if (led_toggle == 0)
	{    
	  debug_printf("MAIN_LOOP_CALLBACK: Switching FRU LED 1 off\n\r");
	  signal_deactivate(&signal_fru1);
	  led_toggle = 1;
	}
	else
	{    
	  debug_printf("MAIN_LOOP_CALLBACK: Switching FRU LED 1 on\n\r");
	  signal_activate(&signal_fru1);
	  led_toggle = 0;
	}
	waitabit = 0;
	*/

	/* Toggle the USER_IO_20 pin */
	signal_t userio_sig = USER_IO_20;

	if (io_sate_mainloop){
		signal_deactivate(&userio_sig);
		io_sate_mainloop = 0;
	} else{
		signal_activate(&userio_sig);
		io_sate_mainloop = 0xFF;
	}
}


unsigned char io_sate_timercback = 0;

/* TIMER_CALLBACK(time, fctname) is called everytime the timer exceed the time value */
TIMER_CALLBACK(1s, usermain_timercback)
{
	/* Toggle the USER_IO_20 pin */
	signal_t userio_sig = USER_IO_20;

	//debug_printf("User callback \n\r");

	if (io_sate_timercback){
		signal_deactivate(&userio_sig);
		io_sate_timercback = 0;
	} else{
		signal_activate(&userio_sig);
		io_sate_timercback = 0xFF;
	}
}
