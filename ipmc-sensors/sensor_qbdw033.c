/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the QBDW033 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_QBDW033

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_qbdw033.h>
#include <i2c_dev.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use QBDW033 sensors.
#endif

#undef DEBUG
//#define DEBUG

/* ------------------------------------------------------------------ */

#define MGMT_I2C_BUS        1
#define QBDW033_I2C_ADDR    MO_CHANNEL_ADDRESS(MGMT_I2C_BUS, 0x1B << 1) /* I2C address of the QBDW033 DC/DC converter */

/* QBDW033 Register definitions */
#define QBDW033_READ_VIN_REG		0x88
#define QBDW033_READ_VOUT_REG		0x8B
#define QBDW033_READ_IOUT_REG		0x8C
#define QBDW033_READ_TEMP1_REG	    0x8D

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_qbdw033_init(sensor_t *sensor);
static char sensor_qbdw033_fill_reading(sensor_t *sensor, unsigned char *msg);

/* ------------------------------------------------------------------ */

/* QBDW033 temperature sensor methods */
sensor_methods_t PROGMEM sensor_qbdw033_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_qbdw033_fill_reading,
    rearm:			&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:			&sensor_qbdw033_init
};

/* Read-only info structures of QBDW033 temperature sensors */
static const sensor_qbdw033_ro_t PROGMEM sensor_qbdw033_ro[] = { CFG_SENSOR_QBDW033 };

#define QBDW033_COUNT	sizeofarray(sensor_qbdw033_ro)

/* Read-write info structures of QBDW033 temperature sensors */
static struct sensor_qbdw033 {
    sensor_threshold_t	sensor;
} sensor_qbdw033[QBDW033_COUNT] WARM_BSS;
typedef struct sensor_qbdw033 sensor_qbdw033_t;

DECL_SENSOR_GROUP(master, sensor_qbdw033_ro, sensor_qbdw033, NULL);

/* Global flags for all QBDW033 sensors */
static unsigned char sensor_qbdw033_global_flags;
#define QBDW033_GLOBAL_INIT	    (1 << 0)	/* initialize all sensors */
#define QBDW033_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */
/*    Internal functions for accessing QBDW033                        */
/* ------------------------------------------------------------------ */

/* Read an QBDW033 register */
static char inline sensor_qbdw033_read_sensor(unsigned char reg, unsigned char *val)
{
    unsigned char data[2];

	*val = 0xFF; // unrealistic value to mark an error
    if (i2c_dev_read_reg(QBDW033_I2C_ADDR, reg, data, 2)) return I2C_ERROR;
	if (reg == QBDW033_READ_VOUT_REG) // output voltage
		// resolution is then 62.5 mV
		*val = data[1];
	else // VOUT, IOUT, TEMP
		// assume the maximum voltage is 64 V -> drop the two MSBs, resolution is 0.25 V
		// Temperature resolution is 0.5 degC
		*val = (data[0] >> 1) | ((data[1] & 1 ) << 7);
#ifdef DEBUG
    debug_printf("QBDW033 @ 0x%03x, register 0x%02x: data = 0x%02x%02x, sensor = %d\n", QBDW033_I2C_ADDR, reg, data[0], data [1], *val);
#endif
    return 0;
}

/* ------------------------------------------------------------------ */

/* 
    The following function updates the reading of the given QBDW033 sensor. 
*/
static void sensor_qbdw033_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char reading;
    /* get the sensor number */
	/* get the channel number */
    unsigned char chan = PRG_RD(sensor_qbdw033_ro[num].sns) & 3;
	unsigned char reg_map[4] = { QBDW033_READ_VIN_REG, QBDW033_READ_VOUT_REG, QBDW033_READ_IOUT_REG, QBDW033_READ_TEMP1_REG };
    if (monly_i2c_is_ready(QBDW033_I2C_ADDR)) {
		/* read temperature or adc */
		if (!sensor_qbdw033_read_sensor(reg_map[chan], &reading)) {
			/* update sensor reading */
			sensor_threshold_update_s(&sensor_qbdw033[num].sensor, reading, flags);
		}
    }
}

/*
    The following function initializes the given
    QBDW033 sensor.
*/
static void sensor_qbdw033_initialize(unsigned char num)
{
//    unsigned char chan = PRG_RD(sensor_qbdw033_ro[num].sns) & 3;

#ifdef DEBUG
    debug_printf("qbdw033 #%d, initialize\n", num);
#endif
    if (monly_i2c_is_ready(QBDW033_I2C_ADDR)) {
		/* read the current sensor value */
		sensor_qbdw033_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_qbdw033_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_qbdw033 *) sensor) - sensor_qbdw033;
    sensor_qbdw033_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
SLAVE_UP_CALLBACK(sensor_qbdw033_slave_up)
{
    /* schedule global QBDW033 init/update */
    sensor_qbdw033_global_flags = QBDW033_GLOBAL_INIT | QBDW033_GLOBAL_UPDATE;
}

SLAVE_DOWN_CALLBACK(sensor_qbdw033_slave_down)
{
    /* unschedule global QBDW033 init/update */
    sensor_qbdw033_global_flags = 0;
}
#endif

#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_qbdw033_carrier_up)
{
    /* schedule global QBDW033 init/update */
    sensor_qbdw033_global_flags = QBDW033_GLOBAL_INIT | QBDW033_GLOBAL_UPDATE;
}

CARRIER_DOWN_CALLBACK(sensor_qbdw033_carrier_down)
{
    /* unschedule global QBDW033 init/update */
    sensor_qbdw033_global_flags = 0;
}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the QBDW033 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_qbdw033_1s_callback)
{
    unsigned char flags;

    /* schedule global QBDW033 update */
    save_flags_cli(flags);
    sensor_qbdw033_global_flags |= QBDW033_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_qbdw033_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global QBDW033 flags */
    save_flags_cli(flags);
    gflags = sensor_qbdw033_global_flags;
    sensor_qbdw033_global_flags = 0;
    restore_flags(flags);

    if (gflags & QBDW033_GLOBAL_INIT) {
	/* make a delay to let the slave/carrier AVRs stabilize */
	udelay(20000);

        /* initialize all QBDW033 */
	for (i = 0; i < QBDW033_COUNT; i++) {
	    if (!(sensor_qbdw033[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_qbdw033_initialize(i);
	    }
	}
    }

    if (gflags & QBDW033_GLOBAL_UPDATE) {
	/* update all sensor readings */
	for (i = 0; i < QBDW033_COUNT; i++) {
	    if (!(sensor_qbdw033[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_qbdw033_update_reading(i, 0);
	    }
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_qbdw033_init_all)
{
    /* schedule global initialization */
    sensor_qbdw033_global_flags = QBDW033_GLOBAL_INIT | QBDW033_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains QBDW033 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_qbdw033_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_qbdw033_update_reading((sensor_qbdw033_t *)sensor - sensor_qbdw033, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_QBDW033 */
