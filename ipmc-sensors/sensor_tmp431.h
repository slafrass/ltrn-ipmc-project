/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_tmp431.c module.

    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_TMP431_H__
#define __MASTER_SENSOR_TMP431_H__

#include <sensor.h>

/* Read-only info structure of an TMP431 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned char bus_sns; /* Slave I2C channel number of PCA9545A switch and local/remote temperature select */
} sensor_tmp431_ro_t;

/* TMP431 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_tmp431_methods;

/* Auxiliary macro for defining TMP431 sensor info */
#define SENSOR_TMP431(s, a, alert) \
    { \
		SA(sensor_tmp431_methods, s, alert), \
		bus_sns: (a) \
    }

#endif /* __MASTER_SENSOR_TMP431_H__ */
