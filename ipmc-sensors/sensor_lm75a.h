/*
    Copyright (c) 2003-2016 Pentair Technical Products, Inc.
    All rights reserved.
    Pentair Technical Products, Inc. proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_ds75.c module.
    
    $Revision: 13620 $
    
    Note: The copyright above refers to the file ds75.h. This file has been created my M. Joos, CERN, on the basis of ds75.h
    
*/

#ifndef __MASTER_SENSOR_LM75A_H__
#define __MASTER_SENSOR_LM75A_H__

#include <sensor.h>

/* Read-only info structure of a LM75A temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned short	i2c_addr;	/* I2C address of the chip */
    unsigned char	irq;		/* IRQ line (0xff = no IRQ) */
} sensor_lm75a_ro_t;

/* LM75A temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_lm75a_methods;

/* Auxiliary macro for defining LM75A sensor info */
#define SENSOR_LM75A(s, a, i, alert)		\
    {						\
	SA(sensor_lm75a_methods, s, alert),	\
	i2c_addr: (a),				\
	irq: (i)				\
    }

#endif /* __MASTER_SENSOR_LM75A_H__ */
