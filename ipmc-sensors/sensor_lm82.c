#define NEED_MASTERONLY_I2C		/* Check that the Sensor_i2c bus is implemented */ 
 
#include <defs.h> 
#include <cfgint.h> 
#include <debug.h> 
 
#ifdef CFG_SENSOR_LM82			/* Compile the source only if at least one SENSOR_LM82 is implemented by the user */ 
 
#include <hal/i2c.h>				/* I2C functions */ 
#include <i2c_dev.h>				/* Master Only I2C functions */ 
#include <hal/system.h>				/* System functions */ 
	 
#include <app.h>					/* App functions */ 
#include <log.h>					/* Log functions */ 
#include <sensor.h>					/* Sensors functions */ 
#include <sensor_discrete.h>		/* Discrete sensor functions */ 
#include <sensor_threshold.h>		/* Threshold related functions */ 
 
#include <sensor_lm82.h>		/* Sensor related header file */ 
 
#ifndef HAS_MASTERONLY_I2C 
    #error Enable master-only I2C support to use LM82 sensors. 
#endif 
 
 
/* ------------------------------------------------------------------ */ 
/* Sensor's methods 				 								  */ 
/* ------------------------------------------------------------------ */ 
 
/* Sensor specific methods */ 
static char sensor_lm82_init(sensor_t *sensor);								/* Function called by the core to initialize the device */ 
 
sensor_methods_t PROGMEM sensor_lm82_methods = { 
    fill_event:		&sensor_threshold_fill_event,		/* Called when the core asks for event */ 
    fill_reading:	&sensor_lm82_fill_rd,			/* Called when the core asks for sensor value */ 
    rearm:			&sensor_threshold_rearm,			/* Called when the core asks for sensor arm */ 
    set_thresholds:	&sensor_threshold_set_thresholds,	/* Called when a new threshold value is forced */ 
    get_thresholds:	&sensor_threshold_get_thresholds,	/* Called when thresholds value are requested */ 
    set_hysteresis:	&sensor_threshold_set_hysteresis,	/* Called to set a new threshold hysteresis value */ 
    get_hysteresis:	&sensor_threshold_get_hysteresis,	/* Called to get the threshold hysteresis value */ 
    init:			&sensor_lm82_init				/* Called when the core asks for sensor init */ 
}; 
 
/* ------------------------------------------------------------------ */ 
/* Memory allocation 												  */ 
/* ------------------------------------------------------------------ */ 
static const sensor_lm82_ro_t PROGMEM sensor_lm82_ro[] = { 
	CFG_SENSOR_LM82 /* Defined in impc-config/config_sensors.h */ 
}; 
 
#define SENSOR_LM82_COUNT	sizeofarray(sensor_lm82_ro) 
 
/* Read-write info structures of LM82 temperature sensors */ 
static struct sensor_lm82 { 
    sensor_threshold_t	sensor; 
} sensor_lm82[SENSOR_LM82_COUNT] WARM_BSS; 
 
typedef struct sensor_lm82 sensor_lm82_t; 
   
 
/* ------------------------------------------------------------------ */ 
/* Sensor declaration												  */ 
/* ------------------------------------------------------------------ */ 
DECL_SENSOR_GROUP(master, sensor_lm82_ro, sensor_lm82, NULL); 
 
/* ------------------------------------------------------------------ */ 
/* Flag variable and state											  */ 
/* ------------------------------------------------------------------ */ 
static unsigned char sensor_lm82_global_flags; 
unsigned char sensor_lm82_initialized[SENSOR_LM82_COUNT]; 
 
#define LM82_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */ 
#define LM82_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */ 
 
/* ------------------------------------------------------------------ */ 
/* This section contains functions specific to the device.			  */ 
/* ------------------------------------------------------------------ */ 
unsigned char initialize_sensor_lm82(unsigned short i2c_addr, unsigned char reg){ 
	// YE: Nothing to initialize
    return 0x00; 
} 
 
unsigned char read_sensor_lm82(unsigned char *value, unsigned short i2c_addr, unsigned char reg){ 

    *value = 0x00; 

    // Check Sensor_I2C bus availability
    if (!monly_i2c_is_ready(i2c_addr)) {
    // print debug message   
    debug_printf("Sensor_I2C bus is not ready\n"); 
    return I2C_NOTREADY;
    }

    // Read Data
    // char i2c_dev_read_reg(unsigned short addr, unsigned char reg, unsigned char *pdata, unsigned char size) 
    unsigned char status;
    status = i2c_dev_read_reg(i2c_addr, reg, value, 1);
    if (status != I2C_OK) {
    // print debug message
    debug_printf("LM82 reading failed\n");
    return status;
    }

    return 0x00; //return non-zero value in case of reading error (sensor not updated)
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains Template sensor methods. 					  */ 
/* ------------------------------------------------------------------ */ 
 
/* Fill the Get Sensor Reading reply */ 
static char sensor_lm82_fill_rd(sensor_t *sensor, unsigned char *msg){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i, sval, error; 
     
    i = ((sensor_lm82_t *) sensor) - sensor_lm82; 
    if (sensor_lm82_initialized[i]) { 
        sensor_lm82_initialized[i] = initialize_sensor_lm82(sensor_lm82_ro[i].i2c_addr, sensor_lm82_ro[i].reg); 
    } 
    error = read_sensor_lm82(&sval, sensor_lm82_ro[i].i2c_addr, sensor_lm82_ro[i].reg); 
     
    /* Update sensor value */ 
    if (error == 0){ 
        sensor_threshold_update_s(&sensor_lm82[i].sensor, sval, 0); 
    } 

    return sensor_threshold_fill_reading(sensor, msg); 
 
} 
 
/* Sensor initialization. */ 
static char sensor_lm82_init(sensor_t *sensor){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_lm82_t *) sensor) - sensor_lm82; 
     
    /* Execute init function */ 
    sensor_lm82_initialized[i] = initialize_sensor_lm82(sensor_lm82_ro[i].i2c_addr, sensor_lm82_ro[i].reg); 
     
    return 0;    
 
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains callbacks used to manage the sensor. 		  */ 
/* ------------------------------------------------------------------ */ 

/* YE: payload power off */
#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_lm82_carrier_up)
{
    /* schedule global LM82 init/update */
    sensor_lm82_global_flags = LM82_GLOBAL_INIT | LM82_GLOBAL_UPDATE;
}
CARRIER_DOWN_CALLBACK(sensor_lm82_carrier_down)
{
    /* unschedule global LM82 init/update */
    sensor_lm82_global_flags = 0;
}
#endif

/* 1 second callback */ 
TIMER_CALLBACK(1s, sensor_lm82_1s_callback){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_lm82_global_flags |= LM82_GLOBAL_UPDATE; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Initialization callback */ 
INIT_CALLBACK(sensor_lm82_init_all){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_lm82_global_flags |= LM82_GLOBAL_INIT; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Main loop callback */ 
MAIN_LOOP_CALLBACK(sensor_lm82_poll){ 
 
    unsigned char i, flags, gflags, pcheck, sval, error; 
 
    /* Disable interrupts */ 
    save_flags_cli(flags); 
 
    /* Saved flag state into a local variable */ 
    gflags = sensor_lm82_global_flags; 
 
    /* Clear flags */ 
    sensor_lm82_global_flags = 0; 
 
    /* Enable interrupts */ 
    restore_flags(flags); 
 
    if (gflags & LM82_GLOBAL_INIT) { 
 
        /* initialize all Template sensors */ 
        for (i = 0; i < SENSOR_LM82_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
            pcheck = sensor_lm82[i].sensor.s.status; 
 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                sensor_lm82_initialized[i] = initialize_sensor_lm82(sensor_lm82_ro[i].i2c_addr, sensor_lm82_ro[i].reg); 
            } else { 
                sensor_lm82_initialized[i] = 0xff;
            } 
		} 
    } 
 
    if (gflags & LM82_GLOBAL_UPDATE) { 
 
    	/* update all sensor readings */ 
        for (i = 0; i < SENSOR_LM82_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
        	pcheck = sensor_lm82[i].sensor.s.status; 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                if (sensor_lm82_initialized[i]) { 
                    sensor_lm82_initialized[i] = initialize_sensor_lm82(sensor_lm82_ro[i].i2c_addr, sensor_lm82_ro[i].reg); 
                } 
                WDT_RESET; 
                error = read_sensor_lm82(&sval, sensor_lm82_ro[i].i2c_addr, sensor_lm82_ro[i].reg); 
                if (error == 0){ 
                    sensor_threshold_update_s(&sensor_lm82[i].sensor, sval, flags); 
                } 
            } 
        } 
    } 
 
} 
 
#endif /* CFG_SENSOR_LM82 */ 

