#define NEED_MASTERONLY_I2C			/* Check that the Sensor_i2c bus is implemented */ 
 
#include <defs.h> 
#include <cfgint.h> 
#include <debug.h> 
 
#ifdef CFG_SENSOR_IQ65033			/* Compile the source only if at least one SENSOR_IQ65033 is implemented by the user */ 
 
#include <hal/i2c.h>				/* I2C functions */ 
#include <i2c_dev.h>				/* Master Only I2C functions */ 
#include <hal/system.h>				/* System functions */ 
	 
#include <app.h>					/* App functions */ 
#include <log.h>					/* Log functions */ 
#include <sensor.h>					/* Sensors functions */ 
#include <sensor_discrete.h>		/* Discrete sensor functions */ 
#include <sensor_threshold.h>		/* Threshold related functions */ 
 
#include <sensor_iq65033.h>			/* Sensor related header file */ 
 
#ifndef HAS_MASTERONLY_I2C 
#error Enable master-only I2C support to use IQ65033 sensors. 
#endif 
 
 
/* ------------------------------------------------------------------ */ 
/* Sensor's methods 				 								  */ 
/* ------------------------------------------------------------------ */ 
 
/* Sensor specific methods */ 
static char sensor_iq65033_init(sensor_t *sensor);		/* Function called by the core to initialize the device */ 
 
sensor_methods_t PROGMEM sensor_iq65033_methods = { 
    fill_event:		&sensor_threshold_fill_event,		/* Called when the core asks for event */ 
    fill_reading:	&sensor_iq65033_fill_rd,			/* Called when the core asks for sensor value */ 
    rearm:			&sensor_threshold_rearm,			/* Called when the core asks for sensor arm */ 
    set_thresholds:	&sensor_threshold_set_thresholds,	/* Called when a new threshold value is forced */ 
    get_thresholds:	&sensor_threshold_get_thresholds,	/* Called when thresholds value are requested */ 
    set_hysteresis:	&sensor_threshold_set_hysteresis,	/* Called to set a new threshold hysteresis value */ 
    get_hysteresis:	&sensor_threshold_get_hysteresis,	/* Called to get the threshold hysteresis value */ 
    init:			&sensor_iq65033_init				/* Called when the core asks for sensor init */ 
}; 
 
/* ------------------------------------------------------------------ */ 
/* Memory allocation 												  */ 
/* ------------------------------------------------------------------ */ 
static const sensor_iq65033_ro_t PROGMEM sensor_iq65033_ro[] = { 
	CFG_SENSOR_IQ65033 /* Defined in impc-config/config_sensors.h */ 
}; 
 
#define SENSOR_IQ65033_COUNT	sizeofarray(sensor_iq65033_ro) 
 
/* Read-write info structures of IQ65033 temperature sensors */ 
static struct sensor_iq65033 { 
    sensor_threshold_t	sensor; 
} sensor_iq65033[SENSOR_IQ65033_COUNT] WARM_BSS; 
 
typedef struct sensor_iq65033 sensor_iq65033_t; 
   
 
/* ------------------------------------------------------------------ */ 
/* Sensor declaration												  */ 
/* ------------------------------------------------------------------ */ 
DECL_SENSOR_GROUP(master, sensor_iq65033_ro, sensor_iq65033, NULL); 
 
/* ------------------------------------------------------------------ */ 
/* Flag variable and state											  */ 
/* ------------------------------------------------------------------ */ 
static unsigned char sensor_iq65033_global_flags; 
unsigned char sensor_iq65033_initialized[SENSOR_IQ65033_COUNT]; 
 
#define IQ65033_GLOBAL_INIT	(1 << 0)		/* initialize all sensors */ 
#define IQ65033_GLOBAL_UPDATE (1 << 1)		/* update all sensors */ 
 
/* ------------------------------------------------------------------ */ 
/* This section contains functions specific to the device.			  */ 
/* ------------------------------------------------------------------ */ 
unsigned char initialize_sensor_iq65033(unsigned short i2c_addr, unsigned char reg){ 
	// YE: Nothing to initialize
    return 0x00; 
} 
 
unsigned char read_sensor_iq65033(unsigned char *value, unsigned short i2c_addr, unsigned char reg){ 

    *value = 0x00; 

    // Read Data
    // char i2c_dev_read_reg(unsigned short addr, unsigned char reg, unsigned char *pdata, unsigned char size) 
    if (i2c_dev_read_reg(i2c_addr, reg, value, 1)) return I2C_ERROR;

    return 0x00; //return non-zero value in case of reading error (sensor not updated)
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains Template sensor methods. 					  */ 
/* ------------------------------------------------------------------ */ 
 
/* Fill the Get Sensor Reading reply */ 
static char sensor_iq65033_fill_rd(sensor_t *sensor, unsigned char *msg){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i, sval, error; 
     
    i = ((sensor_iq65033_t *) sensor) - sensor_iq65033; 
    if (sensor_iq65033_initialized[i]) { 
        sensor_iq65033_initialized[i] = initialize_sensor_iq65033(sensor_iq65033_ro[i].i2c_addr, sensor_iq65033_ro[i].reg); 
    } 
    error = read_sensor_iq65033(&sval, sensor_iq65033_ro[i].i2c_addr, sensor_iq65033_ro[i].reg); 
     
    /* Update sensor value */ 
    if (error == 0){ 
        sensor_threshold_update_s(&sensor_iq65033[i].sensor, sval, 0); 
    } 

    return sensor_threshold_fill_reading(sensor, msg); 
 
} 
 
/* Sensor initialization. */ 
static char sensor_iq65033_init(sensor_t *sensor){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_iq65033_t *) sensor) - sensor_iq65033; 
     
    /* Execute init function */ 
    sensor_iq65033_initialized[i] = initialize_sensor_iq65033(sensor_iq65033_ro[i].i2c_addr, sensor_iq65033_ro[i].reg); 
     
    return 0;    
 
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains callbacks used to manage the sensor. 		  */ 
/* ------------------------------------------------------------------ */ 
 
/* 1 second callback */ 
TIMER_CALLBACK(1s, sensor_iq65033_1s_callback){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_iq65033_global_flags |= IQ65033_GLOBAL_UPDATE; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Initialization callback */ 
INIT_CALLBACK(sensor_iq65033_init_all){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_iq65033_global_flags |= IQ65033_GLOBAL_INIT; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Main loop callback */ 
MAIN_LOOP_CALLBACK(sensor_iq65033_poll){ 
 
    unsigned char i, flags, gflags, pcheck, sval, error; 
 
    /* Disable interrupts */ 
    save_flags_cli(flags); 
 
    /* Saved flag state into a local variable */ 
    gflags = sensor_iq65033_global_flags; 
 
    /* Clear flags */ 
    sensor_iq65033_global_flags = 0; 
 
    /* Enable interrupts */ 
    restore_flags(flags); 
 
    if (gflags & IQ65033_GLOBAL_INIT) { 
 
        /* initialize all Template sensors */ 
        for (i = 0; i < SENSOR_IQ65033_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
            pcheck = sensor_iq65033[i].sensor.s.status; 
 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                sensor_iq65033_initialized[i] = initialize_sensor_iq65033(sensor_iq65033_ro[i].i2c_addr, sensor_iq65033_ro[i].reg); 
            } else { 
                sensor_iq65033_initialized[i] = 0xff;
            } 
		} 
    } 
 
    if (gflags & IQ65033_GLOBAL_UPDATE) { 
 
    	/* update all sensor readings */ 
        for (i = 0; i < SENSOR_IQ65033_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
        	pcheck = sensor_iq65033[i].sensor.s.status; 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                if (sensor_iq65033_initialized[i]) { 
                    sensor_iq65033_initialized[i] = initialize_sensor_iq65033(sensor_iq65033_ro[i].i2c_addr, sensor_iq65033_ro[i].reg); 
                } 
                WDT_RESET; 
                error = read_sensor_iq65033(&sval, sensor_iq65033_ro[i].i2c_addr, sensor_iq65033_ro[i].reg); 
                if (error == 0){ 
                    sensor_threshold_update_s(&sensor_iq65033[i].sensor, sval, flags); 
                } 
            } 
        } 
    } 
 
} 
 
#endif /* CFG_SENSOR_IQ65033 */ 

