/* 
    Description:
    This header file defines the interfaces
    provided by the sensor_iq65033.c module.

    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_IQ65033_H__
#define __MASTER_SENSOR_IQ65033_H__

#include <sensor.h>

/* Read-only info structure of an Template sensor */
typedef struct {
    sensor_ro_t s;
    unsigned short i2c_addr;
    unsigned char reg;
} sensor_iq65033_ro_t;

/* Template sensor methods */
extern sensor_methods_t PROGMEM sensor_iq65033_methods;

static char sensor_iq65033_fill_rd(sensor_t *sensor, unsigned char *msg);

/* Auxiliary macro for defining Template sensor info */
#define SENSOR_IQ65033(s, i2c_addr_p, reg_p, alert)		\
    {						\
        SA(sensor_iq65033_methods, s, alert), \
        i2c_addr: (i2c_addr_p), \
        reg: (reg_p) \
    }
     
#endif /* __MASTER_SENSOR_IQ65033_H__ */
