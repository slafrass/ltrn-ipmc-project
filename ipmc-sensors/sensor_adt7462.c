/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the ADT7462 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_ADT7462

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_adt7462.h>
#include <fru_fan_adt7462.h>
#include <i2c_dev.h>
#include <adt7462.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use ADT7462 sensors.
#endif

#undef DEBUG

#define FREQ		    90000		/* on-chip oscillator */
#define RPM(x)		    (FREQ * 60 / (x))   /* formula from datasheet */
#define SRPM(x)		    (FREQ / (x))	/* opimized formula */

/* Forward declarations */
static char sensor_adt7462_init(sensor_t *sensor);
static char sensor_adt7462_fill_reading(sensor_t *sensor,
	unsigned char *msg);

static unsigned char adt7462_sensor_count;
static time_t adt7462_update_timer;

/* ------------------------------------------------------------------ */

/* ADT7462 sensor methods */
sensor_methods_t PROGMEM sensor_adt7462_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_adt7462_fill_reading,
    rearm:		&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:		&sensor_adt7462_init
};

/* Read-only info structures of ADT7462 sensors */
static const sensor_adt7462_ro_t PROGMEM sensor_adt7462_ro[] =
	{ CFG_SENSOR_ADT7462 };

#define ADT7462_COUNT	sizeofarray(sensor_adt7462_ro)

/* Read-write info structures of ADT7462 sensors */
static struct sensor_adt7462 {
    sensor_threshold_t	sensor;
} sensor_adt7462[ADT7462_COUNT] WARM_BSS;
typedef struct sensor_adt7462 sensor_adt7462_t;

static unsigned short sensor_adt7462_first;
DECL_SENSOR_GROUP(master, sensor_adt7462_ro, sensor_adt7462,
	&sensor_adt7462_first);

/* Global flags for all ADT7462 sensors */
static unsigned char sensor_adt7462_global_flags;
#define ADT7462_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */
#define ADT7462_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    The following function updates the reading
    of the given ADT7462 sensor.
*/
static void sensor_adt7462_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char snum, tach, reading[2] = {0}, tach_indx, rpms;
    unsigned short addr;

    /* get the I2C address of ADT7462, and channel number */
    snum = sensor_adt7462_first + num;
    addr = PRG_RD(sensor_adt7462_ro[num].i2c_addr);
    tach = PRG_RD(sensor_adt7462_ro[num].tach);
    
    if (tach > 3) {
	tach++;
    }
    tach_indx = tach << 1;

    if (monly_i2c_is_ready(addr)) {
	/* read tachometer data */
	if (!(i2c_dev_read_reg(addr, ADT7462_TACH1_LSB_REG + tach_indx,	
		    &reading[1], 1))
		    && !(i2c_dev_read_reg(addr, ADT7462_TACH1_MSB_REG + tach_indx, 
		    &reading[0], 1))) {
	/* update sensor reading */
	if (*(unsigned short *)reading == 0xffff) {
	    rpms = 0;
	} else {
	    rpms = (unsigned char)((SRPM(*(unsigned short *)reading)));
	}
			    
	sensor_threshold_update(&master_sensor_set, 
		    snum, rpms, flags);
	}
    }
}

/*
    The following function initializes the given
    ADT7462 sensor.
*/
static int sensor_adt7462_initialize(unsigned char num)
{
    unsigned char data[2], tach;
    unsigned short addr;

    /* get I2C address of ADT7462 */
    addr = PRG_RD(sensor_adt7462_ro[num].i2c_addr);

    /* get tachometer number */
    tach = PRG_RD(sensor_adt7462_ro[num].tach);

#ifdef DEBUG
    debug_printf("adt7462 #%02x, initialize \n", num);
#endif

    if (monly_i2c_is_ready(addr)) {

	 /* Check adt7462 IDs */ 
	if (i2c_dev_read_reg(addr, ADT7462_DEVICE_ID_REG, &data[0], 1) ||
	i2c_dev_read_reg(addr, ADT7462_COMPANY_ID_REG, &data[1], 1)) {
	    printf("Sensor: can't read ADT7462 ID\n");
	} else if ((data[0] != ADT7462_DEVICE_ID) 
		|| (data[1] != ADT7462_COMPANY_ID)) {
	    printf("Sensor: ADT7462 not found at address: %x\n", addr);
	    return -1;
	}

	/* enable all tachometers tachometers, 0xFF - all tachometers are enabled */
	if (i2c_dev_read_reg(addr, ADT7462_TACH_ENABLE_REG, &data[0], 1)) {
	    printf("Error: can't initialize tach #%x for sensor #%x\n", tach, num);
	    return -1;
	} else {
	    data[0] |= 1 << tach;
	    i2c_dev_write_reg(addr, ADT7462_TACH_ENABLE_REG, &data[0], 1);
	}
	
	data[0] = 0x0f;
	i2c_dev_write_reg(addr, 0x08, &data[0], 1);
	/* Init complete, set setup complete bit */
	if (i2c_dev_read_reg(addr, ADT7462_CFG1_REG, &data[0], 1)) {
	    printf("Error: can't initialize sensor #%x\n", num);
	    return -1;
	} else {
	    data[0] |= ADT7462_CFG1_SETUP_COMPLETE;
	    i2c_dev_write_reg(addr, ADT7462_CFG1_REG, &data[0], 1);
	}

	/* read the current sensor value */
	sensor_adt7462_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
    return 0;
}

/*
    Sensor initialization.
*/
static char sensor_adt7462_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_adt7462 *) sensor) - sensor_adt7462;
    if (sensor_adt7462_initialize(num)) {
	sensor_adt7462[num].sensor.s.status |= STATUS_SENSOR_DISABLE;
    }
    return 0;
}

/* ------------------------------------------------------------------ */
/*
    This section contains various callbacks
    registered by the ADT7462 driver.
*/

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_adt7462_poll)
{
    unsigned char i = 0, flags, gflags;

    /* get/clear global ADT7462 flags */
    save_flags_cli(flags);
    gflags = sensor_adt7462_global_flags;
    sensor_adt7462_global_flags = 0;
    restore_flags(flags);

    if (gflags & ADT7462_GLOBAL_INIT) {
        /* initialize all ADT7462 */
	for (i = 0; i < ADT7462_COUNT; i++) {
	    if (!(sensor_adt7462[i].sensor.s.status & STATUS_SENSOR_DISABLE)) {
		sensor_adt7462_initialize(i);
	    }
	}
    }

    if (gflags & ADT7462_GLOBAL_UPDATE) {

	/* update first sensor reading */
	if (!(sensor_adt7462[i].sensor.s.status & STATUS_SENSOR_DISABLE)) {
	    sensor_adt7462_update_reading(0, 0);
	}

	/* Set counter to next sensor */ 
	adt7462_sensor_count = 1;
    }
    if ((adt7462_sensor_count != 0) 
	    || timer_is_expired(adt7462_update_timer, 
		CFG_SENSOR_ADT7462_UPDATE_PERIOD)) {
	/* update next sensor reading */
	if (!(sensor_adt7462[i].sensor.s.status & STATUS_SENSOR_DISABLE)) {
	    sensor_adt7462_update_reading(adt7462_sensor_count, 0);
	}
	if (++adt7462_sensor_count == ADT7462_COUNT) {
	    adt7462_sensor_count = 0;
	    timer_set(adt7462_update_timer);
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_adt7462_init_all)
{
    /* schedule global initialization */
    sensor_adt7462_global_flags = ADT7462_GLOBAL_INIT | ADT7462_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains ADT7462 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_adt7462_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_adt7462_update_reading(
	    (sensor_adt7462_t *)sensor - sensor_adt7462, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_ADT7462 */
